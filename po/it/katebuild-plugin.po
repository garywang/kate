# translation of katebuild-plugin.po to Italian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Nicola Ruggero <nicola@nxnt.org>, 2009, 2010, 2011, 2012, 2013.
# Federico Zenith <federico.zenith@member.fsf.org>, 2012.
# Vincenzo Reale <smart2128vr@gmail.com>, 2014.
# SPDX-FileCopyrightText: 2016, 2018, 2019, 2020, 2021, 2022, 2023, 2024 Paolo Zamponi <feus73@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: katebuild-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-16 00:40+0000\n"
"PO-Revision-Date: 2024-03-17 19:34+0100\n"
"Last-Translator: Paolo Zamponi <feus73@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.02.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Nicola Ruggero"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "nicola@nxnt.org"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Output"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "Compila di nuovo"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "Annulla"

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr "Aggiungi errori ed avvisi alla diagnostica"

#: buildconfig.cpp:27
#, kde-format
msgid "Automatically switch to output pane on executing the selected target"
msgstr ""
"Passa automaticamente al pannello di output nell'esecuzione dell'obiettivo "
"selezionato"

#: buildconfig.cpp:44
#, kde-format
msgid "Build & Run"
msgstr "Compila ed esegui"

#: buildconfig.cpp:50
#, kde-format
msgid "Build & Run Settings"
msgstr "Impostazioni di Compila ed esegui"

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1232
#, kde-format
msgid "Build"
msgstr "Compila"

#: plugin_katebuild.cpp:222
#, kde-format
msgid "Select Target..."
msgstr "Seleziona obiettivo..."

#: plugin_katebuild.cpp:227
#, kde-format
msgid "Build Selected Target"
msgstr "Compila l'obiettivo selezionato"

#: plugin_katebuild.cpp:232
#, kde-format
msgid "Build and Run Selected Target"
msgstr "Compila ed esegui l'obiettivo selezionato"

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr "Ferma"

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr "Attiva la scheda successiva sulla sinistra"

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr "Attiva la scheda successiva sulla destra"

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Impostazioni obiettivo"

#: plugin_katebuild.cpp:403
#, kde-format
msgid "Build Information"
msgstr "Informazioni di compilazione"

#: plugin_katebuild.cpp:620
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "Non ci sono file o cartelle specificate per la compilazione."

#: plugin_katebuild.cpp:624
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""
"Il file «%1» non è un file locale. Impossibile compilare file non-locali."

#: plugin_katebuild.cpp:686
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""
"Impossibile eseguire il comando: %1\n"
"Il percorso di lavoro non esiste: %2"

#: plugin_katebuild.cpp:700
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "Impossibile avviare «%1». Stato di uscita = %2"

#: plugin_katebuild.cpp:715
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "Compilazione di <b>%1</b> annullata"

#: plugin_katebuild.cpp:822
#, kde-format
msgid "No target available for building."
msgstr "Nessun obiettivo disponibile per la compilazione."

#: plugin_katebuild.cpp:836
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "Non ci sono file o cartelle locali specificate per la compilazione."

#: plugin_katebuild.cpp:842
#, kde-format
msgid "Already building..."
msgstr "Compilazione già in corso..."

#: plugin_katebuild.cpp:864
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "Compilazione obiettivo <b>%1</b> ..."

#: plugin_katebuild.cpp:878
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>Risultati Make:</title><nl/>%1"

#: plugin_katebuild.cpp:914
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "Compilazione di <b>%1</b> terminata. %2 errori, %3 avvisi, %4 note"

#: plugin_katebuild.cpp:920
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Trovato un errore."
msgstr[1] "Trovato %1 errori."

#: plugin_katebuild.cpp:924
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Trovato un avviso."
msgstr[1] "Trovato %1 avvisi."

#: plugin_katebuild.cpp:927
#, kde-format
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "Trovata una nota."
msgstr[1] "Trovate %1 note."

#: plugin_katebuild.cpp:932
#, kde-format
msgid "Build failed."
msgstr "Compilazione non riuscita."

#: plugin_katebuild.cpp:934
#, kde-format
msgid "Build completed without problems."
msgstr "Compilazione conclusa senza problemi."

#: plugin_katebuild.cpp:939
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "Compilazione di <b>%1</b> annullata. %2 errori, %3 avvisi, %4 note"

#: plugin_katebuild.cpp:963
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr "Impossibile eseguire: %1 Nessuna cartella di lavoro impostata."

#: plugin_katebuild.cpp:1189
#, kde-format
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "errore"

#: plugin_katebuild.cpp:1192
#, kde-format
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "avviso"

#: plugin_katebuild.cpp:1195
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr "nota|informazione"

#: plugin_katebuild.cpp:1198
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "riferimento non definito"

#: plugin_katebuild.cpp:1231 TargetModel.cpp:285 TargetModel.cpp:297
#, kde-format
msgid "Target Set"
msgstr "Insieme di obiettivi"

#: plugin_katebuild.cpp:1233
#, kde-format
msgid "Clean"
msgstr "Pulisci"

#: plugin_katebuild.cpp:1234
#, kde-format
msgid "Config"
msgstr "Configura"

#: plugin_katebuild.cpp:1235
#, kde-format
msgid "ConfigClean"
msgstr "ConfigClean"

#: plugin_katebuild.cpp:1426
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr "Impossibile salvare gli obiettivi di compilazione in %1"

#: TargetHtmlDelegate.cpp:50
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>T:</B> %1"

#: TargetHtmlDelegate.cpp:52
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>Dir:</B> %1"

#: TargetHtmlDelegate.cpp:101
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"Lascia vuoto per usare la cartella del documento attuale.\n"
"Aggiungi le cartelle di ricerca aggiungendo i percorsi separati da «;»"

#: TargetHtmlDelegate.cpp:108
#, kde-format
msgid ""
"Use:\n"
"\"%B\" for project base directory\n"
"\"%b\" for name of project base directory"
msgstr ""
"Uso:\n"
"«%B» per la cartella base del progetto\n"
"«%b» per la cartella base del nome del progetto"

#: TargetHtmlDelegate.cpp:111
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Utilizzo:\n"
"\"%f\" per il file attuale\n"
"\"%d\" per la cartella del file attuale\n"
"\"%n\" per il nome del file attuale senza suffisso"

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr "Progetto"

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr "Sessione"

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr "Nome comando/insieme di obiettivi"

#: TargetModel.cpp:627
#, kde-format
msgid "Working Directory / Command"
msgstr "Cartella di lavoro / Comando"

#: TargetModel.cpp:630
#, kde-format
msgid "Run Command"
msgstr "Esegui comando"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""
"Filtra gli obiettivi, usa i tasti freccia per selezionare e Invio per "
"eseguire"

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "Crea nuovo insieme di obiettivi"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "Copia un comando o un insieme di obiettivi"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr "Elimina l'obiettivo attuale o l'insieme attuale degli obiettivi"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "Aggiungi un nuovo obiettivo"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "Compila l'obiettivo selezionato"

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr "Compila ed esegui l'obiettivo selezionato"

#: targets.cpp:52
#, kde-format
msgid "Move selected target up"
msgstr "Sposta l'obiettivo selezionato in alto"

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr "Sposta l'obiettivo selezionato in basso"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&Compila"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "Inserisci percorso"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "Seleziona la cartella da inserire"

#~ msgid "Project Plugin Targets"
#~ msgstr "Obiettivi dell'estensione di progetto"

#~ msgid "build"
#~ msgstr "compila"

#~ msgid "clean"
#~ msgstr "pulisci"

#~ msgid "quick"
#~ msgstr "rapido"

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "Compilazione di <b>%1</b> completata."

#~ msgid "Building <b>%1</b> had errors."
#~ msgstr "Compilazione di <b>%1</b> con errori."

#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "Compilazione di <b>%1</b> con avvisi."

#~ msgid "Show:"
#~ msgstr "Mostra:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "File"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "Riga"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "Messaggio"

#~ msgid "Next Error"
#~ msgstr "Errore successivo"

#~ msgid "Previous Error"
#~ msgstr "Errore precedente"

#~ msgid "Show Marks"
#~ msgstr "Mostra segni"

#~ msgctxt "@info"
#~ msgid ""
#~ "<title>Could not open file:</title><nl/>%1<br/>Try adding a search path "
#~ "to the working directory in the Target Settings"
#~ msgstr ""
#~ "<title>Impossibile aprire il file:</title><nl/>%1<br/>Prova ad aggiungere "
#~ "un percorso di ricerca alla cartella di lavoro nelle Impostazioni "
#~ "obiettivo"

#~ msgid "Error"
#~ msgstr "Errore"

#~ msgid "Warning"
#~ msgstr "Avviso"

#~ msgid "Only Errors"
#~ msgstr "Solo errori"

#~ msgid "Errors and Warnings"
#~ msgstr "Errori ed avvisi"

#~ msgid "Parsed Output"
#~ msgstr "Output compilazione ottimizzato"

#~ msgid "Full Output"
#~ msgstr "Output compilazione completo"

#~ msgid ""
#~ "Check the check-box to make the command the default for the target-set."
#~ msgstr ""
#~ "Marca la casella di selezione per rendere il comando predefinito per "
#~ "l'insieme degli obiettivi."

#~ msgid "Select active target set"
#~ msgstr "Seleziona l'insieme di obiettivi attivo"

#~ msgid "Filter targets"
#~ msgstr "Filtra obiettivi"

#~ msgid "Build Default Target"
#~ msgstr "Compila l'obiettivo predefinito"

#~ msgid "Build and Run Default Target"
#~ msgstr "Compila ed esegui l'obiettivo predefinito"

#~ msgid "Build Previous Target"
#~ msgstr "Compila l'obiettivo precedente"

#~ msgid "Active target-set:"
#~ msgstr "Insieme di obiettivi attivo:"

#~ msgid "config"
#~ msgstr "configura"

#~ msgid "Kate Build Plugin"
#~ msgstr "Estensione di compilazione di Kate"

#~ msgid "Select build target"
#~ msgstr "Seleziona un obiettivo della compilazione"

#~ msgid "Filter"
#~ msgstr "Filtro"

#~ msgid "Build Output"
#~ msgstr "Output della compilazione"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "<title>Make Results:</title><nl/>%1"
#~ msgctxt "@info"
#~ msgid "<title>Could not open file:</title><nl/>%1"
#~ msgstr "<title>Risultati Make:</title><nl/>%1"

#~ msgid "Next Set of Targets"
#~ msgstr "Insieme di target successivo"

#~ msgid "No previous target to build."
#~ msgstr "Nessun target precedente da compilare."

#~ msgid "No target set as default target."
#~ msgstr "Nessun target impostato come predefinito."

#~ msgid "No target set as clean target."
#~ msgstr "Nessun target impostato come target di pulizia"

#~ msgid "Target \"%1\" not found for building."
#~ msgstr "Target \"%1\" non trovato per la compilazione"

#~ msgid "Really delete target %1?"
#~ msgstr "Elimino veramente il target %1?"

#~ msgid "Nothing built yet."
#~ msgstr "Nessuna compilazione eseguita."

#~ msgid "Target Set %1"
#~ msgstr "Insieme di target %1"

#~ msgid "Target"
#~ msgstr "Target"

#~ msgid "Target:"
#~ msgstr "Target:"

#~ msgid "from"
#~ msgstr "da"

#, fuzzy
#~| msgid "Next Target"
#~ msgid "Sets of Targets"
#~ msgstr "Destinazione successiva"

#~ msgid "Make Results"
#~ msgstr "Risultati di Make"

#~ msgid "Others"
#~ msgstr "Altro"

#~ msgid "Quick Compile"
#~ msgstr "Compilazione rapida"

#~ msgid "The custom command is empty."
#~ msgstr "Il comando personalizzato è vuoto."

#~ msgid "New"
#~ msgstr "Nuovo"

#~ msgid "Copy"
#~ msgstr "Copia"

#~ msgid "Delete"
#~ msgstr "Elimina"

#~ msgid "Quick compile"
#~ msgstr "Compilazione rapida"

#~ msgid "Run make"
#~ msgstr "Avvia make"

#~ msgid "Break"
#~ msgstr "Interrompi"
