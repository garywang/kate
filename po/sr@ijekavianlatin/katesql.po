# Translation of katesql.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2010, 2011, 2012, 2014, 2015, 2017.
# Dalibor Djuric <daliborddjuric@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: katesql\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-16 00:40+0000\n"
"PO-Revision-Date: 2017-09-28 17:56+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Associated-UI-Catalogs: kwidgetsaddons5_qt\n"

#: connectionwizard.cpp:28
#, kde-format
msgctxt "@title:window"
msgid "Connection Wizard"
msgstr "Čarobnjak povezivanja"

#: connectionwizard.cpp:43
#, kde-format
msgctxt "@title Wizard page title"
msgid "Database Driver"
msgstr "Drajver baze"

#: connectionwizard.cpp:44
#, kde-format
msgctxt "@title Wizard page subtitle"
msgid "Select the database driver"
msgstr "Izaberite drajver baze"

#: connectionwizard.cpp:51
#, kde-format
msgctxt "@label:listbox"
msgid "Database driver:"
msgstr "Drajver baze:"

#: connectionwizard.cpp:80 connectionwizard.cpp:168
#, kde-format
msgctxt "@title Wizard page title"
msgid "Connection Parameters"
msgstr "Parametri povezivanja"

#: connectionwizard.cpp:81
#, kde-format
msgctxt "@title Wizard page subtitle"
msgid "Please enter connection parameters"
msgstr "Unesite parametre za povezivanje"

#: connectionwizard.cpp:93
#, kde-format
msgctxt "@item Spinbox special value"
msgid "Default"
msgstr "podrazumijevano"

#: connectionwizard.cpp:96
#, kde-format
msgctxt "@label:textbox"
msgid "Hostname:"
msgstr "Ime domaćina:"

#: connectionwizard.cpp:97
#, kde-format
msgctxt "@label:textbox"
msgid "Username:"
msgstr "Korisničko ime:"

#: connectionwizard.cpp:98
#, kde-format
msgctxt "@label:textbox"
msgid "Password:"
msgstr "Lozinka:"

#: connectionwizard.cpp:99
#, kde-format
msgctxt "@label:spinbox"
msgid "Port:"
msgstr "Port:"

#: connectionwizard.cpp:100
#, kde-format
msgctxt "@label:textbox"
msgid "Database name:"
msgstr "Ime baze:"

#: connectionwizard.cpp:101 connectionwizard.cpp:180
#, kde-format
msgctxt "@label:textbox"
msgid "Connection options:"
msgstr "Opcije povezivanja:"

#: connectionwizard.cpp:153
#, kde-format
msgid "Unable to connect to database."
msgstr "Ne mogu da se povežem sa bazom."

#: connectionwizard.cpp:170
#, kde-format
msgctxt "@title Wizard page subtitle"
msgid ""
"Please enter the SQLite database file path.\n"
"If the file does not exist, a new database will be created."
msgstr ""
"Unesite putanju fajla SQLiteove baze.\n"
"Ako fajl ne postoji, biće stvorena nova baza."

# >> @item:inlistbox
#: connectionwizard.cpp:178
#, kde-format
msgid "Database files"
msgstr "fajlovi baze"

# >> @item:inlistbox
#: connectionwizard.cpp:178 exportwizard.cpp:55
#, kde-format
msgid "All files"
msgstr "svi fajlovi"

#: connectionwizard.cpp:179
#, kde-format
msgctxt "@label:textbox"
msgid "Path:"
msgstr "Putanja:"

#: connectionwizard.cpp:212
#, kde-kuit-format
msgctxt "@info"
msgid "Unable to connect to database.<nl/><message>%1</message>"
msgstr "Ne mogu da se povežem sa bazom.<nl/><message>%1</message>"

#: connectionwizard.cpp:227
#, kde-format
msgctxt "@title Wizard page title"
msgid "Connection Name"
msgstr "Ime veze"

#: connectionwizard.cpp:228
#, kde-format
msgctxt "@title Wizard page subtitle"
msgid "Enter a unique connection name"
msgstr "Unesite jedinstveno ime za vezu"

#: connectionwizard.cpp:234
#, kde-format
msgctxt "@label:textbox"
msgid "Connection name:"
msgstr "Ime veze:"

#: dataoutputwidget.cpp:62
#, kde-format
msgctxt "@action:intoolbar"
msgid "Resize columns to contents"
msgstr "Veličina kolona prema sadržaju"

#: dataoutputwidget.cpp:66
#, kde-format
msgctxt "@action:intoolbar"
msgid "Resize rows to contents"
msgstr "Veličina vrsta prema sadržaju"

#: dataoutputwidget.cpp:70
#, kde-format
msgctxt "@action:intoolbar"
msgid "Copy"
msgstr "Kopiraj"

#: dataoutputwidget.cpp:75
#, kde-format
msgctxt "@action:intoolbar"
msgid "Export..."
msgstr "Izvezi..."

#: dataoutputwidget.cpp:80 textoutputwidget.cpp:47
#, kde-format
msgctxt "@action:intoolbar"
msgid "Clear"
msgstr "Očisti"

#: dataoutputwidget.cpp:87
#, kde-format
msgctxt "@action:intoolbar"
msgid "Use system locale"
msgstr "Sistemski lokalitet"

#: dataoutputwidget.cpp:272
#, kde-kuit-format
msgctxt "@info"
msgid "Unable to open file <filename>%1</filename>"
msgstr "Ne mogu da otvorim fajl <filename>%1</filename>."

#: exportwizard.cpp:26
#, kde-format
msgctxt "@title:window"
msgid "Export Wizard"
msgstr "Čarobnjak izvoza"

# rewrite-msgid: /Target/Destination/
#: exportwizard.cpp:41
#, kde-format
msgctxt "@title Wizard page title"
msgid "Output Target"
msgstr "Odredište izlaza"

# rewrite-msgid: /Target/Destination/
#: exportwizard.cpp:42
#, kde-format
msgctxt "@title Wizard page subtitle"
msgid "Select the output target."
msgstr "Izaberite odredište izlaza."

#: exportwizard.cpp:46
#, kde-format
msgctxt "@option:radio Output target"
msgid "Current document"
msgstr "Tekući dokument"

#: exportwizard.cpp:47
#, kde-format
msgctxt "@option:radio Output target"
msgid "Clipboard"
msgstr "Klipbord"

#: exportwizard.cpp:48
#, kde-format
msgctxt "@option:radio Output target"
msgid "File"
msgstr "Fajl"

#: exportwizard.cpp:55
#, fuzzy, kde-format
#| msgid ""
#| "*.csv|Comma Separated Values\n"
#| "*|All files"
msgid "Comma Separated Values"
msgstr ""
"*.csv|vrednosti razdvojene zarezima\n"
"*|svi fajlovi"

#: exportwizard.cpp:97
#, kde-format
msgctxt "@title Wizard page title"
msgid "Fields Format"
msgstr "Format polja"

#: exportwizard.cpp:98
#, kde-format
msgctxt "@title Wizard page subtitle"
msgid ""
"Select fields format.\n"
"Click on \"Finish\" button to export data."
msgstr ""
"Izaberite format polja.\n"
"Kliknite na dugme „Završi“ da izvezete podatke."

#: exportwizard.cpp:102
#, kde-format
msgctxt "@title:group"
msgid "Headers"
msgstr "Zaglavlja"

#: exportwizard.cpp:105
#, kde-format
msgctxt "@option:check"
msgid "Export column names"
msgstr "Izvezi imena kolona"

#: exportwizard.cpp:106
#, kde-format
msgctxt "@option:check"
msgid "Export line numbers"
msgstr "Izvezi brojeve redova"

#: exportwizard.cpp:113
#, kde-format
msgctxt "@title:group"
msgid "Quotes"
msgstr "Navodnici"

#: exportwizard.cpp:116
#, kde-format
msgctxt "@option:check"
msgid "Quote strings"
msgstr "Navodnici oko niski"

#: exportwizard.cpp:117
#, kde-format
msgctxt "@option:check"
msgid "Quote numbers"
msgstr "Navodnici oko brojeva"

#: exportwizard.cpp:125 exportwizard.cpp:128
#, kde-format
msgctxt "@label:textbox"
msgid "Character:"
msgstr "Znak:"

#: exportwizard.cpp:134
#, kde-format
msgctxt "@title:group"
msgid "Delimiters"
msgstr "Razdvajači"

#: exportwizard.cpp:140
#, kde-format
msgctxt "@label:textbox"
msgid "Field delimiter:"
msgstr "Razdvajač polja:"

#: katesqlconfigpage.cpp:23
#, kde-format
msgctxt "@option:check"
msgid "Save and restore connections in Kate session"
msgstr "Sačuvaj i obnovi veze u Kateinoj sesiji"

#: katesqlconfigpage.cpp:25
#, kde-format
msgctxt "@title:group"
msgid "Output Customization"
msgstr "Prilagođavanje izlaza"

#: katesqlconfigpage.cpp:53
#, kde-format
msgctxt "@title"
msgid "SQL"
msgstr "SQL"

#: katesqlconfigpage.cpp:58
#, kde-format
msgctxt "@title:window"
msgid "SQL ConfigPage Settings"
msgstr "Postavke postavne stranice za SQL"

#: katesqlview.cpp:45
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "SQL"
msgid "SQL"
msgstr "SQL"

#: katesqlview.cpp:52
#, fuzzy, kde-format
#| msgctxt "@title"
#| msgid "SQL"
msgctxt "@title:window"
msgid "SQL"
msgstr "SQL"

#: katesqlview.cpp:58
#, fuzzy, kde-format
#| msgctxt "@title:window"
#| msgid "SQL Schema Browser"
msgctxt "@title:window"
msgid "SQL Schema"
msgstr "Pregledač SQL šema"

#: katesqlview.cpp:106
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Add connection..."
msgctxt "@action:inmenu"
msgid "Add Connection..."
msgstr "Dodaj vezu..."

#: katesqlview.cpp:111
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Remove connection"
msgctxt "@action:inmenu"
msgid "Remove Connection"
msgstr "Ukloni vezu"

#: katesqlview.cpp:116
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Edit connection..."
msgctxt "@action:inmenu"
msgid "Edit Connection..."
msgstr "Uredi vezu..."

#: katesqlview.cpp:121
#, kde-format
msgctxt "@action:inmenu"
msgid "Reconnect"
msgstr "Poveži se ponovo"

#: katesqlview.cpp:127
#, kde-format
msgctxt "@action:intoolbar"
msgid "Connection"
msgstr "Veza"

#: katesqlview.cpp:131
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Run query"
msgctxt "@action:inmenu"
msgid "Run Query"
msgstr "Izvrši upit"

#: outputstylewidget.cpp:29
#, kde-format
msgctxt "@title:column"
msgid "Context"
msgstr "kontekst"

#: outputstylewidget.cpp:29
#, kde-format
msgctxt "@title:column"
msgid "Text Color"
msgstr "boja teksta"

#: outputstylewidget.cpp:30
#, kde-format
msgctxt "@title:column"
msgid "Background Color"
msgstr "boja pozadine"

#: outputstylewidget.cpp:39
#, kde-format
msgctxt "@item:intable"
msgid "Text"
msgstr "tekst"

#: outputstylewidget.cpp:40
#, kde-format
msgctxt "@item:intable"
msgid "Number"
msgstr "broj"

#: outputstylewidget.cpp:41
#, kde-format
msgctxt "@item:intable"
msgid "Bool"
msgstr "logička"

#: outputstylewidget.cpp:42
#, kde-format
msgctxt "@item:intable"
msgid "Date & Time"
msgstr "datum i vrijeme"

#: outputstylewidget.cpp:43
#, kde-format
msgctxt "@item:intable"
msgid "NULL"
msgstr "prazno"

#: outputstylewidget.cpp:44
#, kde-format
msgctxt "@item:intable"
msgid "BLOB"
msgstr "binarno"

#: outputwidget.cpp:17
#, kde-format
msgctxt "@title:window"
msgid "SQL Text Output"
msgstr "Izlaz SQL teksta"

#: outputwidget.cpp:18
#, kde-format
msgctxt "@title:window"
msgid "SQL Data Output"
msgstr "Izlaz SQL podataka"

#: schemawidget.cpp:34
#, kde-format
msgctxt "@title:column"
msgid "Database schema"
msgstr "šema baze"

#: schemawidget.cpp:91
#, kde-format
msgctxt "@title Folder name"
msgid "Tables"
msgstr "Tabele"

#: schemawidget.cpp:96
#, kde-format
msgctxt "@title Folder name"
msgid "Views"
msgstr "Pogledi"

#: schemawidget.cpp:110
#, kde-format
msgctxt "@title Folder name"
msgid "System Tables"
msgstr "Sistemske tabele"

#: schemawidget.cpp:270
#, kde-format
msgctxt "@action:inmenu Context menu"
msgid "Select Data"
msgstr ""

#: schemawidget.cpp:273
#, kde-format
msgctxt "@action:inmenu Submenu title"
msgid "Generate"
msgstr "Generiši"

# >> SQL statement
#: schemawidget.cpp:275
#, kde-format
msgid "SELECT"
msgstr "SELECT"

# >> SQL statement
#: schemawidget.cpp:276
#, kde-format
msgid "UPDATE"
msgstr "UPDATE"

# >> SQL statement
#: schemawidget.cpp:277
#, kde-format
msgid "INSERT"
msgstr "INSERT"

# >> SQL statement
#: schemawidget.cpp:278
#, kde-format
msgid "DELETE"
msgstr "DELETE"

#: schemawidget.cpp:282
#, kde-format
msgctxt "@action:inmenu Context menu"
msgid "Refresh"
msgstr "Osvježi"

#: sqlmanager.cpp:349
#, kde-format
msgctxt "@info"
msgid "Query completed successfully"
msgstr "Upit uspešno dovršen"

#: sqlmanager.cpp:352
#, kde-format
msgctxt "@info"
msgid "%1 record selected"
msgid_plural "%1 records selected"
msgstr[0] "izabran %1 slog"
msgstr[1] "izabrana %1 sloga"
msgstr[2] "izabrano %1 slogova"
msgstr[3] "izabran %1 slog"

#: sqlmanager.cpp:356
#, kde-format
msgctxt "@info"
msgid "%1 row affected"
msgid_plural "%1 rows affected"
msgstr[0] "utiče na %1 vrstu"
msgstr[1] "utiče na %1 vrste"
msgstr[2] "utiče na %1 vrsta"
msgstr[3] "utiče na %1 vrstu"

#. i18n: ectx: Menu (SQL)
#: ui.rc:6
#, kde-format
msgid "&SQL"
msgstr "&SQL"

#. i18n: ectx: ToolBar (SQLConnectionsToolBar)
#: ui.rc:17
#, kde-format
msgid "SQL Connections Toolbar"
msgstr "Traka SQL povezivanja"

#. i18n: ectx: ToolBar (SQLToolBar)
#: ui.rc:25
#, kde-format
msgid "SQL Toolbar"
msgstr "SQL traka"

#~ msgid "Kate SQL Plugin"
#~ msgstr "Katein priključak za SQL"

#~ msgctxt "@title:window"
#~ msgid "SQL Results"
#~ msgstr "SQL rezultati"
